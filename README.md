# Leica Camera 5.0 for toco

### Cloning :
- Clone this repo in vendor/xiaomi/miuicamera in your working directory by :
```
git clone https://gitlab.com/yucellmustafa/vendor_xiaomi_miuicamera -b leica-5.0 vendor/xiaomi/miuicamera
```

Make these changes in **sm6150.mk**

**sm6150.mk**
```
# MiuiCamera
$(call inherit-product, vendor/xiaomi/miuicamera/config.mk)
```

Make these changes in **BoardConfigCommon.mk**

**BoardConfigCommon.mk**
```
BUILD_BROKEN_DUP_RULES := true
BUILD_BROKEN_ELF_PREBUILT_PRODUCT_COPY_FILES := true
```
## Credits

### Original mod - https://github.com/a406010503/Miui_Camera

### Original repo - https://gitlab.com/ItzDFPlayer/vendor_xiaomi_miuicamera

## Support

### https://t.me/itzdfplayer_stash <br>

